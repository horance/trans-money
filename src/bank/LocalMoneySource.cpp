#include "bank/LocalMoneySource.h"
#include "bank/Exception.h"
#include "bank/MoneyDestination.h"
#include "bank/Messaging.h"
#include "bank/Balance.h"

void LocalMoneySource::transferMoneyTo(const MoneyDestination& to, const Amount amount)
{
	if (ROLE(Balance).getBalance() < amount)
		THROW(Exception, "insufficient money");

    to.transferMoneyFrom(ROLE(AccountIdInfo), amount);

    ROLE(Balance).decreaseBalance(amount);
    ROLE(Messaging).sendTransferToMsg(to.getAccountId(), amount);
}
