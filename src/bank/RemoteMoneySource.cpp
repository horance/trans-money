#include "bank/RemoteMoneySource.h"
#include "bank/MoneyDestination.h"
#include <iostream>

namespace
{
    void sendTransferMoneyRspMsg(const Amount amount)
    {
        std::cout << "send transfer msg rsp with" << amount << std::endl;
    }
}

void RemoteMoneySource::transferMoneyTo(const MoneyDestination& to, const Amount amount)
{
    to.transferMoneyFrom(ROLE(AccountIdInfo), amount);

    sendTransferMoneyRspMsg(amount);
}
