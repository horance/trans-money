#include "bank/Balance.h"

Balance::Balance(const Amount balance) : balance(balance)
{
}

void Balance::decreaseBalance(const Amount amount)
{
    balance -= amount;
}

void Balance::increaseBalance(const Amount amount)
{
	balance += amount;
}

Amount Balance::getBalance() const
{
	return balance;
}
