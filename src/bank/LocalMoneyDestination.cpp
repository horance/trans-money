#include "bank/LocalMoneyDestination.h"
#include "bank/Balance.h"
#include "bank/Messaging.h"

void LocalMoneyDestination::transferMoneyFrom(const AccountIdInfo& from, const Amount amount) const
{
    ROLE(Balance).increaseBalance(amount);
    ROLE(Messaging).sendTransferFromMsg(from, amount);
}

const AccountIdInfo& LocalMoneyDestination::getAccountId() const
{
    return ROLE(AccountIdInfo);
}