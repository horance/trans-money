#include "bank/Exception.h"

Exception::Exception
    (const char* file, unsigned int line, const std::string & msg)
    : file(file)
    , line(line)
    , msg(msg)
{
}

std::string Exception::getMessage() const
{
	return msg;
}

std::string Exception::getFileName() const
{
	return file;
}

unsigned int Exception::getLine() const
{
	return line;
}

const char *Exception::what() const throw ()
{
	return msg.c_str();
}
