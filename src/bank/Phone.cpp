#include "bank/Phone.h"
#include "bank/AccountIdInfo.h"
#include <iostream>

void Phone::sendTransferToMsg(const AccountIdInfo& to, const Amount amount)
{
    std::cout << "transfer " << amount << " to "   << to.getAccountId()   << std::endl;
}

void Phone::sendTransferFromMsg(const AccountIdInfo& from, const Amount amount)
{
    std::cout << "receive " << amount  << "from " << from.getAccountId() << std::endl;
}
