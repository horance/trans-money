#include "bank/LocalAccount.h"

LocalAccount::LocalAccount(unsigned int id, const Amount amount) 
    : LocalAccountIdInfo(id), Balance(amount)
{
}