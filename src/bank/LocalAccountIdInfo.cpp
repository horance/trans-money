#include "bank/LocalAccountIdInfo.h"

LocalAccountIdInfo::LocalAccountIdInfo(const AccountId id) : id(id)
{
}

AccountId LocalAccountIdInfo::getAccountId() const
{
    return id;
}