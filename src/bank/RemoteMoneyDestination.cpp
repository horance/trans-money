#include "bank/RemoteMoneyDestination.h"
#include "bank/AccountIdInfo.h"
#include "base/Status.h"
#include "bank/Exception.h"
#include <iostream>

namespace
{
    Status sendTransferMoneyReqMsg(const AccountIdInfo& from, const Amount amount)
    {
        std::cout << from.getAccountId() << " send transfer money req msg with amount " << amount << std::endl;
        return SUCCESS;
    }
}

void RemoteMoneyDestination::transferMoneyFrom(const AccountIdInfo& from, const Amount amount) const
{
    if (sendTransferMoneyReqMsg(from, amount) != SUCCESS)
        THROW(Exception, "transfer money msg fail");
}

const AccountIdInfo& RemoteMoneyDestination::getAccountId() const
{
    return ROLE(AccountIdInfo);
}
