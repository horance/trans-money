#ifndef HEF0C8ACF_D3F4_42AC_A293_47ADDFB9FBD8
#define HEF0C8ACF_D3F4_42AC_A293_47ADDFB9FBD8

#include "base/Config.h"

#if __SUPPORT_NULL_PTR
#define __nullptr__ nullptr
#else
#define __nullptr__ 0
#endif

#define __null(ptr) ((ptr) == __nullptr__)
#define __not_null(ptr) ((ptr) != __nullptr__)

#endif
