#ifndef INCL_BASE_STATUS_H_
#define INCL_BASE_STATUS_H_

typedef unsigned int Status;

#define DEF_SUCC_STATUS(status, value) \
	const Status status = value

//////////////////////////////////////////////////////////////////////////
DEF_SUCC_STATUS(SUCCESS,           0);
DEF_SUCC_STATUS(CONTINUE,          1);
DEF_SUCC_STATUS(UNKNOWN_EVENT,     2);
DEF_SUCC_STATUS(RESTART_REQUIRED,  3);
DEF_SUCC_STATUS(STOP,              4);

//////////////////////////////////////////////////////////////
const Status __MIN_FAILED_STATUS = 0x10;

#define __FAILED_STATUS_VALUE(value) (0x80000000 | value)
#define DEF_FAILED_STATUS(status, value) \
	const Status status = __FAILED_STATUS_VALUE(value)

DEF_FAILED_STATUS(FAILED,          0);
DEF_FAILED_STATUS(FATAL_BUG,       1);
DEF_FAILED_STATUS(TIMEDOUT,        2);
DEF_FAILED_STATUS(DUPTID,          3);
DEF_FAILED_STATUS(OUT_OF_SCOPE,    4);
DEF_FAILED_STATUS(FORCE_STOPPED,   5);
DEF_FAILED_STATUS(NOTHING_CHANGED, 6);
DEF_FAILED_STATUS(INVALID_DATA,    7);

//////////////////////////////////////////////////////////////
#define IS_FAILED_STATUS(status) ((status) >= __MIN_FAILED_STATUS)
#define IS_SUCC_STATUS(status) (!IS_FAILED_STATUS(status))

#endif
