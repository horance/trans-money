#ifndef HAD7564B1_186F_4342_A8A4_700B73568116
#define HAD7564B1_186F_4342_A8A4_700B73568116

#if defined(__GNUC__)
#define __likely(expr)   __builtin_expect(!!(expr), 1)
#define __unlikely(expr) __builtin_expect(!!(expr), 0)
#else
#define __likely(expr)   !!(expr)
#define __unlikely(expr) !!(expr)
#endif

#endif
