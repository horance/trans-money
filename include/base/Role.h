#ifndef INCL_BASE_ROLE_H_
#define INCL_BASE_ROLE_H_

#include "base/Keywords.h"

/////////////////////////////////////////////////////////////////////////////
#define ROLE(role) get##role()

/////////////////////////////////////////////////////////////////////////////
#define __ROLE_PROTO_TYPE(role) role& ROLE(role) const

/////////////////////////////////////////////////////////////////////////////
#define USE_ROLE(role) ABSTRACT(__ROLE_PROTO_TYPE(role))

/////////////////////////////////////////////////////////////////////////////
#define IMPL_ROLE(role)                                       \
__ROLE_PROTO_TYPE(role)                                       \
{                                                             \
   return const_cast<role&>(static_cast<const role&>(*this)); \
}

namespace details
{
    template <typename T>
    struct Role
    {
    	virtual ~Role() {}
    };
}

/////////////////////////////////////////////////////////////////////////////
#define DEF_ROLE(type) struct type : details::Role<type>

#endif
