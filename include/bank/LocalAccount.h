#ifndef INCL_LOCAL_ACCOUNT_H_
#define INCL_LOCAL_ACCOUNT_H_

#include "bank/LocalAccountIdInfo.h"
#include "bank/Balance.h"
#include "bank/Phone.h"
#include "bank/LocalMoneySource.h"
#include "bank/LocalMoneyDestination.h"

struct LocalAccount
    : private LocalAccountIdInfo
    , private Balance
    , private Phone
    , LocalMoneySource
    , LocalMoneyDestination
{
    LocalAccount(unsigned int id, const Amount amount);

private:
    IMPL_ROLE(AccountIdInfo);
    IMPL_ROLE(Balance);
    IMPL_ROLE(Messaging);
};

#endif
