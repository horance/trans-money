#ifndef INCL_LOCAL_MONEY_SOURCE_H_
#define INCL_LOCAL_MONEY_SOURCE_H_

#include "bank/MoneySource.h"

struct AccountIdInfo;
struct Balance;
struct Messaging;

struct LocalMoneySource : MoneySource
{
private:
    OVERRIDE(void transferMoneyTo(const MoneyDestination& to, const Amount amount));

private:
    USE_ROLE(AccountIdInfo);
    USE_ROLE(Balance);
    USE_ROLE(Messaging);
};

#endif
