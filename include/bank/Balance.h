#ifndef INCL_BALANCE_H_
#define INCL_BALANCE_H_

#include "bank/Amount.h"

struct Balance
{
    explicit Balance(const Amount balance = 0);

	void decreaseBalance(const Amount amount);
	void increaseBalance(const Amount amount);

	Amount getBalance() const;

private:
	Amount balance;
};

#endif
