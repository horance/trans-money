#ifndef INCL_REMOTE_MONEY_DESTINATION_H_
#define INCL_REMOTE_MONEY_DESTINATION_H_

#include "bank/MoneyDestination.h"

struct AccountIdInfo;

struct RemoteMoneyDestination : MoneyDestination
{
    OVERRIDE(void transferMoneyFrom(const AccountIdInfo& from, const Amount amount) const);
    OVERRIDE(const AccountIdInfo& getAccountId() const);

private:
    USE_ROLE(AccountIdInfo);
};

#endif
