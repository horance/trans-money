#ifndef INCL_MESSAGING_H_
#define INCL_MESSAGING_H_

#include "base/Role.h"
#include "bank/Amount.h"

struct AccountIdInfo;

DEF_ROLE(Messaging)
{
	ABSTRACT(void sendTransferToMsg(const AccountIdInfo& to, const Amount amount));
	ABSTRACT(void sendTransferFromMsg(const AccountIdInfo& from, const Amount amount));
};

#endif
