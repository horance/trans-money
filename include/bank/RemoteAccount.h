#ifndef INCL_REMOTE_ACCOUNT_H_
#define INCL_REMOTE_ACCOUNT_H_

#include "bank/RemoteAccountIdInfo.h"
#include "bank/RemoteMoneySource.h"
#include "bank/RemoteMoneyDestination.h"

struct RemoteAccount
    : private RemoteAccountIdInfo
    , RemoteMoneySource
    , RemoteMoneyDestination
{
private:
    IMPL_ROLE(AccountIdInfo);
};

#endif
