#ifndef INCL_LOCAL_ACCOUNT_ID_INFO_H_
#define INCL_LOCAL_ACCOUNT_ID_INFO_H_

#include "bank/AccountIdInfo.h"

struct LocalAccountIdInfo : AccountIdInfo
{
    explicit LocalAccountIdInfo(const AccountId id);

private:
    OVERRIDE(AccountId getAccountId() const);

private:
    const AccountId id;
};

#endif
