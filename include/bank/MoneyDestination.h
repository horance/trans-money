#ifndef INCL_MONEY_DESTINATION_H_
#define INCL_MONEY_DESTINATION_H_

#include "base/Role.h"
#include "bank/Amount.h"

struct AccountIdInfo;

DEF_ROLE(MoneyDestination)
{
	ABSTRACT(void transferMoneyFrom(const AccountIdInfo& from, const Amount amount) const);
	ABSTRACT(const AccountIdInfo& getAccountId() const);
};

#endif
