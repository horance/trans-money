#ifndef INCL_ACCOUNT_REPOSITORY_H_
#define INCL_ACCOUNT_REPOSITORY_H_

#include "base/Role.h"
#include "bank/AccountId.h"

struct MoneySource;

DEF_ROLE(AccountRepository)
{
    static AccountRepository& getInstance();

    ABSTRACT(MoneySource* retrieve(const AccountId id));
    ABSTRACT(MoneySource* create(const AccountId id));
};

#endif
