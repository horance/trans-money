#ifndef INCL_INSUFFICIENT_MONEY_EXCEPTION_H_
#define INCL_INSUFFICIENT_MONEY_EXCEPTION_H_

#include <exception>
#include <string>
#include "base/Keywords.h"

struct Exception : std::exception
{
	Exception(const char* file, unsigned int line, const std::string & msg);

	std::string  getMessage() const;
	std::string  getFileName() const;
	unsigned int getLine() const;

	OVERRIDE(~Exception() throw()) {}
	OVERRIDE(const char *what() const throw ());

private:
	std::string file;
	unsigned line;
	std::string msg;
};

#define THROW(except, msg) throw except(__FILE__, __LINE__, msg)

#endif
