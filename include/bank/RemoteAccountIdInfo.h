#ifndef INCL_REMOTE_ACCOUNT_ID_INFO_H_
#define INCL_REMOTE_ACCOUNT_ID_INFO_H_

#include "bank/AccountIdInfo.h"

struct RemoteAccountIdInfo : AccountIdInfo
{
private:
    OVERRIDE(AccountId getAccountId() const);
};

#endif
