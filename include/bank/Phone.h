#ifndef INCL_PHONE_H_
#define INCL_PHONE_H_

#include "bank/Messaging.h"

struct Phone : Messaging
{
private:
    OVERRIDE(void sendTransferToMsg(const AccountIdInfo& to, const Amount amount));
    OVERRIDE(void sendTransferFromMsg(const AccountIdInfo& from, const Amount amount));
};

#endif
