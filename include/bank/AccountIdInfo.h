#ifndef INCL_ACCOUNT_ID_INFO_H_
#define INCL_ACCOUNT_ID_INFO_H_

#include "base/Role.h"
#include "bank/AccountId.h"

DEF_ROLE(AccountIdInfo)
{
	ABSTRACT(AccountId getAccountId() const);
};

#endif
