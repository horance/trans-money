#ifndef INCL_REMOTE_MONEY_SOURCE_H_
#define INCL_REMOTE_MONEY_SOURCE_H_

#include "bank/MoneySource.h"

struct AccountIdInfo;

struct RemoteMoneySource : MoneySource
{
private:
    OVERRIDE(void transferMoneyTo(const MoneyDestination& to, const Amount amount));

private:
    USE_ROLE(AccountIdInfo);
};

#endif
