#ifndef INCL_LOCAL_MONEY_DESTINATION_H_
#define INCL_LOCAL_MONEY_DESTINATION_H_

#include "bank/MoneyDestination.h"

struct Balance;
struct Messaging;

struct LocalMoneyDestination : MoneyDestination
{
private:
    OVERRIDE(void transferMoneyFrom(const AccountIdInfo& from, const Amount amount) const);
    OVERRIDE(const AccountIdInfo& getAccountId() const);

private:
    USE_ROLE(AccountIdInfo);
    USE_ROLE(Balance);
    USE_ROLE(Messaging);
};

#endif
