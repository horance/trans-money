#ifndef INCL_MONEY_SOURCE_H_
#define INCL_MONEY_SOURCE_H_

#include "base/Role.h"
#include "bank/Amount.h"

struct MoneyDestination;

DEF_ROLE(MoneySource)
{
    ABSTRACT(void transferMoneyTo(const MoneyDestination& to, const Amount amount));
};

#endif
